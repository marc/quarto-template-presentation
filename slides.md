# Index

- Why Kotlin?
- Examples *filter()* and *flatMap()*

---

# Why Kotlin?

- Supports multiple paradigms (OOP, FP, etc.)
- Expressiveness

```Java
data class Person(
  var id: Long,
  var name: String,
  var age: Int,
)
```
- Modern, concise, and safe (Type safety and null safety)
- Easy to learn (e.g: [KorGE](https://korge.org))
- Language of the industry (Google, Twitter, Reddit, Netflix, etc.)
- Interoperable (JVM ecosystem, Spring Boot, etc.)
- Tooling and Learning Materials ([kotlinlang.org](https://kotlinlang.org))

---

# Example *filter()* Java vs Kotlin

## Java
```Java
List<Integer> filterGreaterThan2() {
  return Stream.of(1, 2, 3, 4)
    .filter(num -> num > 2)
    .toList();
}
```
## Kotlin
```Java
fun filterGreaterThan2(): List<Int> {
  return listOf(1, 2, 3, 4).filter {it > 2}
}
```

---

# Example *flatMap()* Java vs Kotlin

\scriptsize

## Java

```Java
List<Integer> flatMap() {
  return Stream.of(Arrays.asList(2, 3, 4), Arrays.asList(1, 2, 2, 3, 4))
    .flatMap(Collection::stream)
    .map(value -> value + 5)
    .toList();
}
```
## Kotlin

```Java
fun flatMap(): List<Int> {
  return listOf(listOf(2, 3, 4), listOf(1, 2, 2, 3, 4))
    .flatMap { list: List<Int> -> list + 5 }
}
```
