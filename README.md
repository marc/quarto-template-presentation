# Quarto Presentation Template

My Quarto template for presentations.

## Requirements

- [Quarto](https://quarto.org/docs/get-started)

## Run (Live Reload)

`quarto preview slides.md`

## Output

The output file (by default a PDF) is in the `_output` directory.
